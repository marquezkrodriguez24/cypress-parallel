const { defineConfig } = require("cypress");
// const getspecFiles = require("cypress-gitlab-parallel-runner");
// const getspecFiles = require('./cypress-parallel')

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      /*RUTA DONDE ESTARAN LOS TEST QUE SERAN EJECUTADO EN
      PARALELO
       En el package.json tiene que ir si o si a como se paramtriza aqui
       example: cypress/e2e/Tests/tmp/parallel.cy.js
      */
      // getspecFiles("./cypress/e2e", true);      
      return config;
    },
  },
});
