describe("Demo Hooks", () => {
  before(() => {
    cy.log("Before");
  });

  beforeEach(() => {
    cy.log("beforeEach");
  });

  it("Should be test #1", () => {
    console.log("Test #1");
  });

  it("Should be test #2", () => {
    console.log("Test #2");
  });

  it.skip("Should be test #3", () => {
    console.log("Test #3");
    expect(1+1).to.equal(2)
  });

  afterEach(() => {
    cy.log("afterEach");
  });

  after(() => {
    cy.log("after");
  });
});
